DROP TABLE IF EXISTS Employee;

CREATE TABLE Employee(
  empCode varchar(250)  PRIMARY KEY,
  empName VARCHAR(250) NOT NULL,
  address VARCHAR(250) 
);


create table project(projCode varchar(250) , projName varchar2(250), emp_code varchar(250) references employee(empCode) on delete set null);


INSERT INTO Project(projCode, projName, emp_code) VALUES
  ('P201', 'PUMA', 'E101'),
  ('P201', 'ATLAS', 'E101'),
  ('P202', 'ELBS', 'E102');


INSERT INTO Employee(empCode, empName, address) VALUES
  ('E101', 'Vinay', 'Nangal'),
  ('E102', 'Ridhima', 'HP'),
  ('E103', 'Geetika', 'PB');


create table department(deptCode varchar(250) , deptName varchar2(250), projCode varchar2(250),emp_code varchar(250) references employee(empCode) on delete set null);


INSERT INTO Department(deptCode, deptName,projCode, emp_code) VALUES
  ('D301', 'CS', 'P201','E101'),
  ('D302', 'HR', 'P201','E102'),
  ('D303', 'Finance','P202', 'E103');
  ('D303', 'Finance', P202','E101');

