package com.altimetric.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Project {

	@Id
	@Column(name="projcode")
	private String projCode;	

	@Column(name="projname")
	private String projName;
	
	@Column(name="emp_code")
	private String empCode;
	
	
	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="emp_code",referencedColumnName="empcode") private Employee
	 * employee;
	 */
	 
	
	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="deptCode") private Department department;
	 */
	
	/*
	 * public Employee getEmployee() { return employee; }
	 * 
	 * public void setEmployee(Employee employee) { this.employee = employee; }
	 */


	public String getProjCode() {
		return projCode;
	}

	/*
	 * public Employee getEmployee() { return employee; }
	 * 
	 * public void setEmployee(Employee employee) { this.employee = employee; }
	 */


	public void setProjCode(String projCode) {
		this.projCode = projCode;
	}
	
	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public String getProjName() {
		return projName;
	}

	public void setProjName(String projName) {
		this.projName = projName;
	}
	
	
}
