package com.altimetric.beans;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import com.sun.istack.NotNull;

@Entity
public class Employee {
	
	@Id
	@Column(name="empcode")
	private String empCode;
	
	@NotNull
	@Column(name="empname")
	private String name;
	
	@Column(name="address")
	private String address;

	
	  @OneToMany	  
	  @JoinColumn(name="emp_code") 
	  private List<Project> projects;
	 
	
	
	
	  @OneToMany	  
	  @JoinColumn(name="emp_code")
	  private List<Department> departments;
	 
	
	
	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}


	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Employee [empCode=" + empCode + ", name=" + name + ", address=" + address + "]";
	}
	
	
	

}
