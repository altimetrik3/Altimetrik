package com.altimetric.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.sun.istack.NotNull;

@Entity
public class Department {

	@Id
	@Column(name="deptcode")
	private String deptCode;
	
	@NotNull
	@Column(name="deptname")
	private String deptName;
	
	
	@Column(name="projcode")
	private String projCode;
	
    
	public String getProjCode() {
		return projCode;
	}

	public void setProjCode(String projCode) {
		this.projCode = projCode;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	@Override
	public String toString() {
		return "Department [deptName=" + deptName + ", deptCode=" + deptCode + ", getDeptName()=" + getDeptName()
				+ ", getDeptCode()=" + getDeptCode() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
	
}
