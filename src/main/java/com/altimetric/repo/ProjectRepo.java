package com.altimetric.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.altimetric.beans.Project;

@Repository
public interface ProjectRepo extends JpaRepository<Project, String>{
	
	Integer deleteProjectByProjCodeAndEmpCode(String projCode,String empCode);

}
