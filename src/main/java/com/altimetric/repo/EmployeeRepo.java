package com.altimetric.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.altimetric.beans.Employee;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, String> {

	@Query(value="select e from Employee e where empCode in ( select empCode from Project where projcode= ?1)")
	List<Employee> findEmployeeByProcCode(String projCode);
}
