package com.altimetric.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.altimetric.beans.Employee;
import com.altimetric.repo.EmployeeRepo;
import com.altimetric.repo.ProjectRepo;

@Service
public class EmployeeService {
	
	@Autowired
	EmployeeRepo empRepo;
	
	@Autowired
	ProjectRepo projRepo;
	
	public List<Employee> getEmployeesByProjCode(String projCode){
		return empRepo.findEmployeeByProcCode(projCode);
	}
	
	@Transactional
	public void deleteEmpForProject(String empCode,String projCode){
		projRepo.deleteProjectByProjCodeAndEmpCode(projCode, empCode);
		//return "done";
	}
		
	
	public List<Employee> getAllEmployees(){
		return empRepo.findAll();
	}

}
