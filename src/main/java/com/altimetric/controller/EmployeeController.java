package com.altimetric.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.altimetric.beans.Employee;
import com.altimetric.service.EmployeeService;

@RestController
@RequestMapping("/api/v1")
public class EmployeeController {
	
	@Autowired
	EmployeeService empService;

	@GetMapping("/ping")
	public String ping() {
		
		return "pong";
	}
	
	@GetMapping("/employees/{projCode}")
	public List<Employee> getEmployeesByProject(@PathVariable ("projCode") String projCode ) {		
		return empService.getEmployeesByProjCode(projCode);
	}
	
	@GetMapping("/employees")
	public List<Employee> getAllEmployees( ) {		
		return empService.getAllEmployees();
	}
	
	@DeleteMapping("/employees/{empCode}/{projCode}")
	public void updateEmployeesForProject(@PathVariable ("projCode") String projCode,@PathVariable ("empCode") String empCode ) {		
		 empService.deleteEmpForProject(empCode,projCode);
	}
}
